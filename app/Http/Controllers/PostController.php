<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditPostFormRequest;
use App\Http\Requests\PostsFormRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        return PostResource::collection($posts);
    }

    public function search(Request $request)
    {
    $keyword = $request->query('q');
    $results = Post::where('name', 'like', "%$keyword%")->paginate(10);
    return PostResource::collection($results);
    }



   
    public function store(PostsFormRequest $request)
    {
        
        $post = new Post();
        $post->name = $request['name'];
        $post->description = $request['description'];
        $post->category = $request['category'];
        if ($request->hasFile('thumbnail')) {
            $imagePath = $request->file('thumbnail')->store('images', 'public');
            $post->thumbnail = $imagePath;
        }
        $post->save();

        return response()->json(['message' => 'Post created successfully'], 201);
    }

    
    public function show(string $id)
    {
        $post = Post::find($id);

        if (!$post) {
            return response()->json(['error' => 'Post not found'], 404);
        }
    
        return new PostResource($post);
    }

    
    
    public function update(EditPostFormRequest $request, string $id)
    {
        
        $post = Post::findOrFail($id);
        $post->name = $request['name'];
        $post->description = $request['description'];
        $post->category = $request['category'];
        if ($request->hasFile('thumbnail')) {
            $imagePath = $request->file('thumbnail')->store('images', 'public');
            $post->thumbnail = $imagePath;
        }
        $post->save();

        return response()->json(['message' => 'Post updated successfully'], 200);
    }

    
    public function destroy(string $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return response()->json(['message' => 'Post deleted successfully'], 200);
    }

    public function upload(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // max 2MB
        ]);

        $imagePath = $request->file('image')->store('images', 'public');

        $image = new Post();
        $image->path = $imagePath;
        $image->save();

        return response()->json(['message' => 'Image uploaded successfully'], 201);
    }
}
