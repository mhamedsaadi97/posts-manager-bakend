# Posts Manager APIs 

Welcome to Posts Manager APIs! This project serves as the backend API for a React frontend application. Follow the steps below to set up the Laravel API project and start the development server.

## Getting Started

To get started with the Laravel API project, follow these steps:

1. **Clone the Repository**: 
   Clone this repository to your local machine
   
2. **Navigate to the Project Directory**: 
   Change your current directory to the project directory

3. **Install Dependencies**: 
   Install the required dependencies for the Laravel API using Composer.
   composer install

4. **Set Up Environment Configuration**: 
   Create a copy of the .env.example file and name it .env. Update the necessary environment variables, such as database connection details.

5. **Create Database**: 
   Create a MySQL database with the name 'posts-manager' (or any other preferred name) in your MySQL server.

6. **Run Migrations**: 
   un database migrations to create the necessary tables in the database:
   php artisan migrate

6. **Start the Development Server:**: 
   After setting up the database and migrations, start the Laravel development server using the following command:
   php artisan serve


   The Laravel API will now be running at http://localhost:8000.

   
